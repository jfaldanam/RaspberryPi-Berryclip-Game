.include "configuration.inc"
.include "Jinter.s"
.include "gameconfig.s"

@Made by: jfaldanam and Javier Perez Abad 

/*
REGISTRIES
r0, r1 --> trash
r2 --> routines parameters
r3 -> led comprobation                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
r4 --> sound
r5 -> score
r6->sound frequency
r7->sound lenght
r8, r9 --> sound trash
r10 -> trash for led comprobation
r11 --> time
r12 ->  nothing*/


.text

@Raspberry Pi3 code
/*
mrs r0,cpsr
mov     r0, #0b11010011   @ Mode SVC, FIQ&IRQ disable
msr spsr_cxsf,r0
add r0,pc,#4
msr ELR_hyp,r0
eret

*/

@ Vector Table inicialization
	mov r0,#0
	ADDEXC 0x18, buttons

@ Stack init for IRQ mode 	
	mov     r0, #0b11010010   
	msr     cpsr_c, r0
	mov     sp, #0x8000
@ Stack init for FIQ mode 	
	mov     r0, #0b11010001
	msr     cpsr_c, r0
	mov     sp, #0x4000        
@ Stack init for SVC mode
	mov     r0, #0b11010011
	msr     cpsr_c, r0
	mov     sp, #0x8000000


@set Up
main:	ldr     r0, =GPBASE 
		mov   r1, #0b01100
		str      r1,[r0,#GPFEN0]

		ldr     r0, =INTBASE 
		ldr    r1,  =0x00100000
		str      r1,[r0,#INTENIRQ2]

		mov   r1, #0b01010011
		msr    cpsr_c, r1
		
		mov r5, #0
		ldr r11, =initialTime
	

@main loop
		
round:	ldr r1, =ledDirection
		cmp r1, #0
		beq round0
		bne round1



round0:	ldr r2, =GLED2
		ldr r3, =GLED2
		bl turnOn
		
		bl wait
		
		ldr r2, =GLED2
		bl turnOff
			
		ldr r2, =GLED1
		ldr r3, =GLED1
		bl turnOn
		
		bl wait
		
		ldr r2, =GLED1
		bl turnOff
			
		ldr r2, =YLED2
		ldr r3, =YLED2
		bl turnOn
		
		bl wait
		
		ldr r2, =YLED2
		bl turnOff
			
		ldr r2, =YLED1
		ldr r3, =YLED1
		bl turnOn
		
		bl wait
		
		ldr r2, =YLED1
		bl turnOff
			
		ldr r2, =RLED2
		ldr r3, =RLED2
		bl turnOn
		
		bl wait
		
		ldr r2, =RLED2
		bl turnOff
			
		ldr r2, =RLED1
		ldr r3, =RLED1
		bl turnOn
		
		bl wait
		
		ldr r2, =RLED1
		bl turnOff
	
		bl wait
		
		b puntuation
		
		
round1: 	ldr r2, =RLED1
		ldr r3, =RLED1
		bl turnOn
		
		bl wait
		
		ldr r2, =RLED1
		bl turnOff
			
		ldr r2, =RLED2
		ldr r3, =RLED2
		bl turnOn
		
		bl wait
		
		ldr r2, =RLED2
		bl turnOff
			
		ldr r2, =YLED1
		ldr r3, =YLED1
		bl turnOn
		
		bl wait
		
		ldr r2, =YLED1
		bl turnOff
			
		ldr r2, =YLED2
		ldr r3, =YLED2
		bl turnOn
		
		bl wait
		
		ldr r2, =YLED2
		bl turnOff
			
		ldr r2, =GLED1
		ldr r3, =GLED1
		bl turnOn
		
		bl wait
		
		ldr r2, =GLED1
		bl turnOff
			
		ldr r2, =GLED2
		ldr r3, =GLED2
		bl turnOn
		
		bl wait
		
		ldr r2, =GLED2
		bl turnOff
	
		bl wait
		
		b puntuation

end:		b end



@IRQ handler
buttons:		ldr     r0, =GPBASE 
			mov   r1, #0b01100
			str      r1,[r0,#GPEDS0]
			
			ldr     r0, =GPBASE 
			mov   r1, #0b01100
			str      r1,[r0,#GPFEN0]

			ldr     r0, =INTBASE 
			ldr    r1,  =0x00100000
			str      r1,[r0,#INTENIRQ2]

			mov   r1, #0b01010011
			msr    cpsr_c, r1

			ldr    r0, =GPBASE 
			ldr    r1, [r0,#GPLEV0] @ldr r1, =BUTTONS
			tst    r1,#0b00100 @tst r1, #BTN2
			
			beq reset
			bne pressed
@When btn2 pressed
reset:		ldr r2, =ALLLEDS
			bl turnOff
			
			bl wait
			
			b main
@When btn1 pressed
pressed:		ldr r1, =ledDirection
			cmp r1, #0
			beq pressed0
			bne pressed1
		
pressed0:		bl wait

			ldr r0, =TON
			ldr r0, [r0]

			ldr r1, =RLED1
			subs r10, r1, r3
			beq nearly
			
			ldr r1, =RLED2
			subs r10, r1, r3
			beq success
			
			ldr r1, =YLED1
			subs r10, r1, r3
			beq nearly

			ldr r1, =YLED2
			subs r10, r1, r3
			beq fail

			ldr r1, =GLED1
			subs r10, r1, r3
			beq fail
			
			ldr r1, =GLED2
			subs r10, r1, r3
			beq fail
			
			b end

pressed1:		bl wait

			ldr r0, =TON
			ldr r0, [r0]

			ldr r1, =RLED1
			subs r10, r1, r3
			beq fail
			
			ldr r1, =RLED2
			subs r10, r1, r3
			beq fail
			
			ldr r1, =YLED1
			subs r10, r1, r3
			beq fail

			ldr r1, =YLED2
			subs r10, r1, r3
			beq nearly

			ldr r1, =GLED1
			subs r10, r1, r3
			beq success
			
			ldr r1, =GLED2
			subs r10, r1, r3
			beq nearly
			
			b end




success:		
			ldr r0, =successPoints
			add r5, r0, r5
			
			ldr r2, =ALLLEDS
			bl turnOff

			ldr r6, =MI
			bl initSound
			
			@bl wait
			
			ldr r0, =successDecrease
			cmp r11,r0
			beq end
			sub r11, r11, r0
			
			b round


nearly:		
			ldr r0, =nearlyPoints
			add r5, r0, r5
			
			
			ldr r2, =ALLLEDS
			bl turnOff


			ldr r6, =SOL
			bl initSound
			
			@bl wait
			
			ldr r0, =nearlyDecrease
			cmp r11,r0
			beq end
			sub r11, r11, r0
			
			b round




fail:			ldr r2, =ALLLEDS
			bl turnOff
			
			ldr r6, =DOAGUDO
			bl initSound
			
			b puntuation





@Turn on  r2 --> led	
turnOn:		ldr r0, =TON
			str r2, [r0]
			bx lr
			
@Turn off r2 --> led
turnOff:		ldr r0, =TOFF
			str r2, [r0]
			bx lr


@ Time for the rounds r11 -> time
wait:			ldr r0, =CTRCLO
			ldr r0, [r0]
			add r1, r0, r11
looptime:		ldr r0, =CTRCLO
			ldr r0, [r0]
			cmp r0, r1
			blt looptime
			bx lr






@Shows the score of the game r5->score
puntuation:	

			
			ldr r0, =TON
			mov r2, #0b100000
			
				
			and r3, r5, r2
			cmp r3, r2
			ldr r1, =RLED1
			streq r1, [r0]

			lsr r2, r2, #1

			and r3, r5, r2
			cmp r3, r2
			ldr r1, =RLED2
			streq r1, [r0]

			lsr r2, r2, #1

			and r3, r5, r2
			cmp r3, r2
			ldr r1, =YLED1
			streq r1, [r0]

			lsr r2, r2, #1

			and r3, r5, r2
			cmp r3, r2
			ldr r1, =YLED2
			streq r1, [r0]

			lsr r2, r2, #1

			and r3, r5, r2
			cmp r3, r2
			ldr r1, =GLED1
			streq r1, [r0]

			lsr r2, r2, #1

			and r3, r5, r2
			cmp r3, r2
			ldr r1, =GLED2
			streq r1, [r0]
						
			b end



@Plays a sound r6->frequency, r7->Duration //Trash on r4,r8,r9
initSound:		
			ldr r0, =CTRCLO
			ldr r0, [r0]
			ldr r1, =500000
			add r7, r0, r1
			ldr r8, =TON
			ldr r9, =TOFF
			
			ldr r2, =BUZZ
			ldr r4, =0


playSound:

			eors r4, #1
			streq r2, [r8]
			strne r2, [r9]
			b waitSound
			b playSound



waitSound:	ldr r0, =CTRCLO
			ldr r0, [r0]
			add r1, r0, r6
looptimeSound:	
			ldr r0, =CTRCLO
			ldr r0, [r0]
			cmp r0, r1
			blt looptimeSound
			
			cmp r0,r7
			blt playSound
			bx lr





	


