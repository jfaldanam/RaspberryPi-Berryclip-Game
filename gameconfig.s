

		@ Make your own game!

		@Decide the timings
		
			@Length of the game
			
				.set	initialTime, 500000
			
			@Increase the speed of the game in case of:
			
				@ A nearly hit
				.set	nearlyDecrease, 50000
				@ A successful hit
				.set  successDecrease, 25000
			
		@How many points do yo want to get in case of:
		
			@ A nearly hit
			.set  nearlyPoints, 1
			@ A successful hit
			.set  successPoints, 2
		
		@ Finally, what direcction do you want to play in?
			
			@ 0 -> green to red led
			@ 1 -> red to green led
			.set ledDirection, 1
	