# RaspberryPi-Berryclip-Game
Simple game for testing assembly programming on a Raspberry Pi with a Berryclip


If you are running the code on a Raspberry Pi 3, make sure to uncomment the code near the start of the `mygame.s` file, under the `@Raspberry Pi3 code` comment.
